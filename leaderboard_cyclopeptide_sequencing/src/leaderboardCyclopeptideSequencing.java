import com.sun.deploy.security.DeployClientAuthCertStore;
import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by urvishparikh on 12/6/13.
 */
public class leaderboardCyclopeptideSequencing {
    public static int[] possibleSpectrum =
            {57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186};


    public static ArrayList<cycloPeptide> branch(ArrayList<cycloPeptide> leaderBoard){
        ArrayList<cycloPeptide> result = new ArrayList<cycloPeptide>();
        for (int i = 0; i < leaderBoard.size(); i++){
            cycloPeptide tempPeptide = new cycloPeptide(leaderBoard.get(i));

        }
        for (int i = 0; i < possibleSpectrum.length; i++){

                for (int k =0; k < leaderBoard.size(); k++)
                {
                    leaderBoard.get(k).addPeptide(possibleSpectrum[i]);
                    result.add(leaderBoard.get(k));


                }

        }
        return result;
    }

    public static void bound(ArrayList<cycloPeptide> leaderBoard){
        for (int i = 0; i < leaderBoard.size(); i++){
            leaderBoard.get(i).generateScore();

        }

    }

    public static void leaderBoardCyclopeptide(Integer[] massSpectrum,int N){
        int totalMass = Collections.max(Arrays.asList(massSpectrum));
        ArrayList<cycloPeptide> leaderBoard = new ArrayList<cycloPeptide>();
        ArrayList<Integer> pep = new ArrayList<Integer>();

        for (int i = 0; i < possibleSpectrum.length; i++){
            leaderBoard.add(new cycloPeptide(new ArrayList<Integer>(Arrays.asList(massSpectrum))));
        }
        while (leaderBoard.size() > 0)
        {
            cycloPeptide leaderPeptide = new cycloPeptide(new ArrayList<Integer>(Arrays.asList(massSpectrum)));
            leaderBoard = branch(leaderBoard);
            bound(leaderBoard);
        }





    }

    public static void main(String[] args) throws IOException {
        String fileName = "/Users/urvishparikh/IdeaProjects/coursera_bioinformatics/leaderboard_cyclopeptide_sequencing/testData/testData.txt";
        ArrayList<String> lines = stepic.parseStepicFile(fileName);
        int N = Integer.parseInt(lines.get(0));
        ArrayList<Integer> massSpectrum = new ArrayList<Integer>();
        for (int i = 1; i < lines.size(); i++){
            String[] spec_line = lines.get(i).split(" ");
            for (String str: spec_line){
                massSpectrum.add(Integer.parseInt(str));
            }
        }

        Integer[] massSpectrumarr = new Integer[massSpectrum.size()];

        leaderBoardCyclopeptide(massSpectrum.toArray(massSpectrumarr), N);

    }



}
