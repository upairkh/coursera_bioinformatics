import com.sun.java.swing.plaf.motif.resources.motif_fr;
import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.util.*;

/**
 * Created by urvishparikh on 12/3/13.
 */

public class gibbsSampler {
    private int k;
    private int t;
    private int n;
    private String[] dna;
    public gibbsSampler(int k, int t, int n, ArrayList<String> dna){
        this.k = k;
        this.t = t;
        this.n = n;
        assert dna.size() == t;
        this.dna =  dna.toArray(new String[t]);
    }


        private motif[] deepCopyArray(motif[] input){
            motif[] result = new motif[input.length];
            for (int i = 0; i < input.length; i++){
                result[i] = input[i];
            }
            return result;
        }

        public motifsScore run(){
            motif[] randmotifs = new motif[t];
            Random rgenerator = new Random();
            for(int i = 0; i < dna.length; i++){
                int r = rgenerator.nextInt(dna[i].length() - k+1);
                randmotifs[i] = new motif(dna[i].substring(r, r+k));

            }
            assert randmotifs.length == dna.length;
            motifsScore motifsWithScore = new motifsScore(randmotifs);
            motifsScore bestMotifs = new motifsScore(deepCopyArray(randmotifs));
            int bestScore = bestMotifs.getScore();


            rgenerator = new Random();
            for (int i =0; i < n; i++){

                int randomIndex = rgenerator.nextInt(dna.length);
                motif[] removedIndexMotifs = new motif[motifsWithScore.getMotifs().length -1];
                if (randomIndex > 0 && randomIndex < dna.length -1) {
                    motif[] beg = Arrays.copyOfRange(motifsWithScore.getMotifs(), 0, randomIndex);
                    motif[] end = Arrays.copyOfRange(motifsWithScore.getMotifs(), randomIndex +1, dna.length);
                    removedIndexMotifs = (motif[]) ArrayUtils.addAll(beg, end);
                }
                else if(randomIndex == 0){
                    removedIndexMotifs =  Arrays.copyOfRange(motifsWithScore.getMotifs(), randomIndex+1, dna.length);
                }
                else if (randomIndex == dna.length-1){
                    removedIndexMotifs =  Arrays.copyOfRange(motifsWithScore.getMotifs(), 0, dna.length -1);
                }
                else {
                    System.out.println("Something went horribly wrong");
                }
                HashMap<Character, Double>[] profile =  motifsScore.generateProfile(removedIndexMotifs, 1);
                motif pmostMotif = motif.profileMostMotif(profile, dna[randomIndex], k);
                assert pmostMotif != null;

                motifsWithScore.getMotifs()[randomIndex] = pmostMotif;
                motifsWithScore.generateScore();

                if (motifsWithScore.getScore() < bestScore){
                    bestMotifs = new motifsScore(deepCopyArray(motifsWithScore.getMotifs()));
                    bestScore = bestMotifs.getScore();
                }

            }
            return bestMotifs;



        }







    public static void main(String[] args) throws IOException {
        String fileName = "../testData/dataset_43_4 (1).txt";
        ArrayList<String> lines = stepic.parseStepicFile(fileName );
        String [] splitFirstLine = lines.get(0).split(" ");
        int k = Integer.parseInt(splitFirstLine[0]);
        int t = Integer.parseInt(splitFirstLine[1]);
        int n = Integer.parseInt(splitFirstLine[2]);
        lines.remove(0);
        gibbsSampler gsampler = new gibbsSampler(k, t, n, lines );
        motifsScore bestMotifs = null;
        for (int i = 0; i < 20; i++){
            motifsScore potentialBestMotifs = gsampler.run();
            if (bestMotifs == null || bestMotifs.getScore() > potentialBestMotifs.getScore()){
                bestMotifs = potentialBestMotifs;
            }
        }

        for (int i = 0; i < bestMotifs.getMotifs().length; i++){
            System.out.println(bestMotifs.getMotifs()[i].getMotif());
        }


    }
}
