import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

/**
 * Created by urvishparikh on 12/4/13.
 */
public class motifsScore {

    private motif [] motifs;
    private int score;
    private String consensus;
    private int motifLength = 0;

    public motifsScore(motif[] motifs){
        motifLength =  motifs[0].getK();
        for (motif m: motifs) assert m.getK() == motifLength;

        this.motifs = motifs;
        this.score = 0;
        this.consensus = null;
        generateScore();
    }

    public int getScore() {
        return score;
    }

    public motif[] getMotifs() {
        return motifs;
    }

    public void generateScore(){
        generateConsensus();
        this.score = 0;

        for (motif m: this.motifs){
            this.score+=StringUtils.getLevenshteinDistance(m.getMotif(), this.consensus); //LevenshteinDistance is hammingDistance
        }
    }

    public static HashMap<Character, Integer>[] generateCounts(motif[] motifs){
        HashMap<Character, Integer> counter;
        int motifLength = motifs[0].getK();
        HashMap<Character, Integer>[] result = new HashMap[motifLength];
        for (int i = 0; i < motifLength; i++){
            counter = new HashMap<Character, Integer>(4);
            counter.put('A', 0);
            counter.put('C', 0);
            counter.put('T', 0);
            counter.put('G', 0);
            for(motif m: motifs){
                counter.put(m.getMotif().charAt(i), counter.get(m.getMotif().charAt(i)) + 1);
            }
            result[i] = counter;

        }
        return result;
    }

    private void generateConsensus(){
        this.consensus = "";
        HashMap<Character, Integer>[] motifCounts =  generateCounts(this.motifs);
        for (int i = 0; i < motifLength; i++){

            HashMap<Character, Integer> counter = motifCounts[i];
            Map.Entry<Character, Integer> maxEntry = null;

            for (Map.Entry<Character, Integer> entry: counter.entrySet()){
                if (maxEntry == null || entry.getValue().compareTo((Integer) maxEntry.getValue()) > 0){
                    maxEntry = entry;
                }
            }
            this.consensus+=maxEntry.getKey();

        }
    }



    public static HashMap<Character, Double>[] generateProfile(motif[] motifs, int pseudocounts){
        HashMap<Character, Integer>[] motifCounts =  generateCounts(motifs);
        double devisor = motifs.length * 2.0;
        HashMap<Character, Double>[] results = new HashMap[motifCounts.length];
        for (int i = 0; i < motifCounts.length; i++){
            HashMap<Character, Integer>  motifCount = motifCounts[i];
            HashMap<Character, Double> result = new HashMap<Character, Double>();
            for (Map.Entry<Character, Integer> entry: motifCount.entrySet()){
                result.put(entry.getKey(), ((Double)entry.getValue().doubleValue() + pseudocounts)/ devisor);
            }
            results[i] = result;
        }
        return results;
    }


}
