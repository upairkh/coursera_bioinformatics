/**
 * Created by urvishparikh on 12/3/13.
 */



public class kmer {
    private String kmer;
    private int k;

    public static kmer[] generateKmers(String text,int k){
        kmer[] results = new kmer[text.length() - k+1];

        for (int i = 0; i < text.length() - k +1; i++){
            results[i] = new kmer(text.substring(i, i+k));

        }
        return results;
    }

    public kmer(String kmer){
        this.kmer = kmer;
        this.k = kmer.length();
    }




    public int getK() {
        return k;
    }

    public String getKmer() {
        return kmer;
    }
}
