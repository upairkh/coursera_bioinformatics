import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by urvishparikh on 12/3/13.
 */
public class motif extends kmer{

    private String motif;



    public motif(String dna){
        super(dna);
        this.motif = dna;

    }
    public String getMotif(){return motif;}
    public void setMotif(String motif){this.motif = motif;}


    private static Double kmerScore(kmer kmer, HashMap<Character, Double>[] profile){
        Double probability = 1.0;
        for (int i = 0; i< kmer.getK(); i++){
            probability *= profile[i].get(kmer.getKmer().charAt(i));
        }
        return probability;
    }

    public static motif profileMostMotif(HashMap<Character, Double>[] profile, String dna,int k){

        HashMap<motif, Double> kmerProbability = new HashMap<motif, Double>();
        kmer[] kmers = generateKmers(dna, k);
        Double total = 0.0;
        for (kmer km: kmers){
            Double score = kmerScore(km, profile);
            kmerProbability.put(new motif(km.getKmer()), score);
            total+= score;
        }
        Random r = new Random();
        double rand = r.nextDouble() * total;
        double upto = 0.0;
        for (Map.Entry<motif, Double> entry: kmerProbability.entrySet()){
            if (upto+ entry.getValue() > rand)
                return entry.getKey();
            upto+= entry.getValue();

        }

        return null;
    }

}
