/**
 * Created by urvishparikh on 12/3/13.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class stepic {

    public static ArrayList<String> parseStepicFile(String filename) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filename));

        ArrayList<String> result = new ArrayList<String>();
        String line;
        while ((line = br.readLine()) != null){
            if (Pattern.matches("Input.*", line))
                continue;
            else if (Pattern.matches("Output.*", line))
                break;
            result.add(line);
        }
        return result;
    }
}
